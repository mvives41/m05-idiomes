package com.company;

// TODO: 18/11/2019 Buscar tots els usos de diesLaborals2XX, per utilitzar-hi també Constants.ESPAI 
public class Textos {
    private String diesLaborals1CA = "La setmana te ";
    private String diesLaborals2CA = "dies laborals.";

    private String diesLaborals1ES = "La semana tiene ";
    private String diesLaborals2ES = "dias laborales.";

    private String diesLaborals1EN = "The week has ";
    private String diesLaborals2EN = "working days.";

    private static String NO_CONTROLAT = "Idioma no controlat";


    public String getDiesLaborals1CA() {
        return diesLaborals1CA;
    }

    public String getDiesLaborals2CA() {
        return diesLaborals2CA;
    }

    public String getDiesLaborals1ES() {
        return diesLaborals1ES;
    }

    public String getDiesLaborals2ES() {
        return diesLaborals2ES;
    }

    public String getDiesLaborals1EN() {
        return diesLaborals1EN;
    }

    public String getDiesLaborals2EN() {
        return diesLaborals2EN;
    }

    public String fraseDiesLaborals(String idioma, int diesLaborals){
        // TODO: 18/11/2019 modificar NO_CONTROLAT -> IDIOMA_NO_CONTROLAT;
        String frase = NO_CONTROLAT;


        switch (idioma) {
            case Constants.ESPANYOL:
                frase = diesLaborals1ES + diesLaborals + diesLaborals2ES;
                break;
            // TODO: 18/11/2019 Afegir control per altres idiomes CA i EN
            default:
                break;
        }
        return frase;
    }
}
